'''A Program used to handle student sign ins to RSO events'''

from os.path import expanduser

import sys
import gspread

from oauth2client.service_account import ServiceAccountCredentials
from pygnin.user_input import parse
from pygnin.user_input import InputError
from email_class import Email

def main():
    ''' A bit of a disclaimer gspread indexes starting at 1
     Row 1 will be reserved for labeling purposes '''

    # local storage for sign ins
    student_signins = dict()

    # Limits the amount of access that is granted on this current OAuth2 access token
    scope = [r'https://spreadsheets.google.com/feeds',
             r'https://www.googleapis.com/auth/drive']

    # Credentials are stored in a JSON file, ask the president for a key.
    # Note that this program requires an Editor level API Key
    path = expanduser("~/.config/pygnin/client_cred.json")
    creds = ServiceAccountCredentials.from_json_keyfile_name(r'' + path, scope)
    client = gspread.authorize(creds)

    project = client.open(r'test thing')


    sheet_index = int(input("Welcome to WuLUG's Pygnin Which sheet would you like to open? "))

    # get_worksheets returns None type when it can't find a sheet
    sheet = project.get_worksheet(sheet_index)
    #sloppy work but someone else can refactor it later.
    sheet = validate_worksheet(sheet, project, sheet_index)

    line = str()
    data = list()

    while line.upper() != 'EXIT':
        try:
            line = input('Enter a student ID and Major: ')
        except EOFError:
            break

        if line == 'exit':
            break

        try:
            # breaks the information within "line" down to a list of arguments for
            # the gspread library
            student, major = extract_data(line)
        except InputError:
            print('there was an issue parsing your input try again')
        else:
            # sheet.append_row(data)
            # print(f'Student ID {data[0]} Major {data[1]}')
            student_signins[student] = major

    for student, major in student_signins.items():
        print(f'{student} {major}')
        sheet.append_row((student, major))

    # defaults to Yes on blank inputs
    emails = input('Would you like to pull emails? [Y/n]') or 'Y'

    if emails.upper() == 'Y':
        email_archive = open(r'../emails', 'w')

        raw_ids = sheet.col_values(1)
        email = Email(raw_ids)

        for address in email.addresses():
            email_archive.write(address + '\n')


def extract_data(line):
    ''' ensures that users don't enter invalid strings '''
    _id, _major = parse(line)

    if _id is None or _major is None:
        raise InputError('Invalid user input')

    return (_id, _major)


def validate_worksheet(worksheet, project, index):
    '''Validates if a specific worksheet is valid or not'''
    sheet = worksheet

    if worksheet is None:
        print("That sheet hasn't been created yet")
        create_sheet = input("would you like to create it? [y/N]") or 'N'

        if create_sheet.upper() == 'Y':
            index += 1
            print(f'Worksheet {index} has been created')
            sheet = project.add_worksheet(title=f'Sheet{index}', rows='1', cols='5')

        else:
            if create_sheet.upper() not in ('Y', 'N'):
                print('An unknown command was entered')

            print('Closing down Pygnin')

            sys.exit()

    return sheet



if __name__ == '__main__':
    main()
