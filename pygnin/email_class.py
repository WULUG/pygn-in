''' this module is meant to help with making and pulling emails '''

class Email:

    def __init__(self,students):
        '''creates a list of students' email addresses and stores it in emails'''
        self.emails = list()
        self.pull_emails(students)

    def addresses(self):
        '''returns a list of the students' email addresses''' 
        return self.emails


    def make_email(self, student, domain='@wichita.edu'):
        ''' takes a student id string and appends an email domain '''
        return str(student + domain)


    def pull_emails(self, students):
        ''' loops through a list of student emails and constructs
        a new list with the @wichita.edu string attached '''

        for student in students:
            if student in ('Student ID', 'Boiler Plate'):
                continue

            self.emails.append(self.make_email(student))
