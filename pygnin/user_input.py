''' this module is meant to help handle user input
for the pygn in program '''
def main():
    ''' the input expected is <student id> <major> '''
    line = input('Enter a student ID and Major: ')

    _id, _major = parse(line)

    print(f'Student ID {_id} Major {_major}')


class InputError(Exception):
    ''' Custom exception for handling input errors
    for the parse function '''


def valid_id(_id):
    ''' returns either true or false; checks for invalid
    characters in student id '''
    if _id:

        if alphabetic(_id[0]) and alphabetic(_id[4]):
            if numeric(_id[1:3]) and numeric(_id[5:7]):
                return True
        else:
            return False

    return False


def alphabetic(char):
    ''' return either true or false and is only
    good for one character at a time '''
    return char.upper() in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def numeric(triplet):
    ''' casts triplet into an integer and returns
    False on when it is unable to do so. '''
    try:
        return int(triplet) in range(1000)
    except ValueError:
        return False


def parse(line):
    ''' parses user input for student id and major '''
    _id = None
    if line:
        _id = line.split(' ')[0]

    if not valid_id(_id):
        _id = 'Non-WSU Student'

    _major = 'Undecided'

    if len(line.split(' ')) > 1:

        _major = line.split(' ')[1]

        # Most Engineering Majors are multiple words
        if len(line.split(' ')) > 2:
            _major += ' ' # append a space first
            _major += line.split(' ')[2]

    return (_id, _major)


if __name__ == '__main__':
    main()
