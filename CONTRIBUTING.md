# Contributing to Pygn In

This is a set of guidelines for contributuing to Pygn In, which is hosted on the GitLab of the Wichita State University Linux Users Group (WuLug). If you wish for changes to be made for these guidelines, include them in your pull request.


# Table of Contents

[1. Making changes](https://gitlab.com/WULUG/pygn-in/blob/master/CONTRIBUTING.md#1-making-changes)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1a. Cloning and Making your own branch](https://gitlab.com/WULUG/pygn-in/blob/master/CONTRIBUTING.md#1a-cloning-and-making-your-own-branch)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1b. Pylint](https://gitlab.com/WULUG/pygn-in/blob/master/CONTRIBUTING.md#1b-pylint)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1c. Merge Requests](https://gitlab.com/WULUG/pygn-in/blob/master/CONTRIBUTING.md#1c-merge-requests)  
[2. Feature requests](https://gitlab.com/WULUG/pygn-in/blob/master/CONTRIBUTING.md#2-feature-requests)  

# 1. Making Changes

Changes made to Pygn In must be done on a seperate branch cloned from the master branch, and have a pylint score of at least 7. Merging your branch with the master must get the approval of two WuLug officers.

# 1a. Cloning and Making your own branch

To clone the master branch form the terminal, retrieve the master branch with:

`git clone https://gitlab.com/WULUG/pygn-in.git`  

Once this is done, change to the root directory of the project. From there you will create a new branch with:  

`git checkout -b <name-of-branch>`

From there, you can start to add or modify files to your cloned repository.

# 1b. Pylint

For checking with pylint, use pylint3:  

`pylint3 <filename.py>`

The pylint score must be 7 or greater to be accepted.

# 1c. Merge Requests

To merge your changes, first you must add it (while you are in your branch):

`git add <filename>`  

then commit it:

`git commit -m <message>`

Then, push your branch:

`git push origin <your branch>`

If your push is over HTTPS, you need to make a Personal Access Token; This will be your password.

# 2. Feature Requests  

To request a feature, please submit an [issue](https://gitlab.com/WULUG/pygn-in/issues) regarding the feature you wish to be added.




