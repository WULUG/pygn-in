# Dependencies

You really only need OAuth2Client and Gspread
virtual environments will cover the rest

```
Package        Version    
-------------- -----------
certifi        2019.6.16  
chardet        3.0.4      
gspread        3.1.0      
httplib2       0.13.1     
idna           2.8        
oauth2         1.9.0.post1
oauth2client   4.1.3      
pip            19.2.3     
pyasn1         0.4.6      
pyasn1-modules 0.2.6      
requests       2.22.0     
rsa            4.0        
setuptools     41.2.0     
six            1.12.0     
urllib3        1.25.3     
wheel          0.33.6
```
# Installation

* Clone the repository
```bash
git clone https://gitlab.com/wulug/pygn-in/
```
* Use pip3 to install pygn-in
```bash
pip3 install pygn-in/
```

# Usage
* Put the client_cred.json in ~/.config/pygnin/client_cred.json
* Inside main.py edit line 25 with your spreadsheet name
* Execute the program
```bash
pygnin
```
