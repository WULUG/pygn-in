#!/usr/bin/env python3

from setuptools import setup
from setuptools import find_packages

setup(name='pygnin',
      version='0.1',
      description='Sign-in program for your organization',
      url='https://gitlab.com/wulug/pygn-in',
      packages=find_packages(),
      install_requires=['wheel','gspread', 'oauth2client'],
      entry_points={
          'console_scripts':[
              'pygnin=pygnin:main.main'
          ]})
